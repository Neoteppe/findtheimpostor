﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameramovement : MonoBehaviour
{
    public float speedH = 2.0f;
    public float speedV = 2.0f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;

    private float movementSpeed = 10;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.W))
		{
            transform.position += transform.forward * movementSpeed * Time.deltaTime;
		}if(Input.GetKey(KeyCode.S))
		{
            transform.position -= transform.forward * movementSpeed * Time.deltaTime;
		}if(Input.GetKey(KeyCode.A))
		{
            transform.position -= transform.right * movementSpeed * Time.deltaTime;
		}if(Input.GetKey(KeyCode.D))
		{
            transform.position += transform.right * movementSpeed * Time.deltaTime;
		}
        if(Input.GetMouseButton(1))
		{
            yaw += speedH * Input.GetAxis("Mouse X");
            pitch -= speedV * Input.GetAxis("Mouse Y");
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
		}
        if(Input.GetMouseButtonUp(1))
		{
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
		}
        if(Input.GetKey(KeyCode.LeftShift))
            movementSpeed = 30;
        if (Input.GetKeyUp(KeyCode.LeftShift))
            movementSpeed = 10;

    }
}
