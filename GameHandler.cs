﻿using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
	public CivilianSpawnPoint[] civilianSpawnPoints;
	public List<GameObject> civilians = new List<GameObject>();

	private void Awake()
	{
		civilianSpawnPoints = FindObjectsOfType<CivilianSpawnPoint>();
	}

	public void SpawnCivilian()
	{
		var go = civilians[Random.Range(0, civilians.Count)];
		var spawnpoint = civilianSpawnPoints[Random.Range(0, civilianSpawnPoints.Length)];
		var civilian = Instantiate(go, spawnpoint.transform.position, Quaternion.identity) as GameObject;
		civilian.name = "Civilian";
		
	}
}

