﻿using UnityEngine;

public interface INode
{
	Vector3 NodeTransform();
	bool Condition();
}