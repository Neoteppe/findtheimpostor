﻿using UnityEngine;

public class Waypoint : MonoBehaviour, INode
{
	public bool Condition()
	{
		return true;
	}

	public Vector3 NodeTransform()
	{
		return this.transform.position;
	}
}
