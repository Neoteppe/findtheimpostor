﻿
using System.Collections.Generic;
using UnityEngine;

namespace Assets.AI.Entity
{
	public class GroupNode : MonoBehaviour, INode
	{
		private const int MAXGROUPCOUNT = 5;
		public int CurrentGroupCount { get; set; } = 0;
		private List<Civilian> Group { get; set; } = new List<Civilian>(MAXGROUPCOUNT);
		private readonly int[,] GroupPositionForCivilian = new int[MAXGROUPCOUNT, MAXGROUPCOUNT];

		public bool Condition()
		{
			return CurrentGroupCount < MAXGROUPCOUNT;
		}
		public void AddCivilianToGroup(Civilian civilian)
		{
			Group.Add(civilian);
		}
		public void RemoveCivilianFromGroup(Civilian civilian)
		{

		}

		public Vector3 NodeTransform()
		{
			return this.transform.position;
		}
	}
}