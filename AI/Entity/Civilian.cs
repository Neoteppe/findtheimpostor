﻿using Assets.AI.States;
using System;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.AI.Entity
{
	public class Civilian : MonoBehaviour
	{
		private StateMachine _stateMachine;
		public INode TargetNode { get; set; }
		public int WaitTimer { get; set; } = 0;
		public bool IsInteracting { get; set; } = false;

		public void Awake()
		{
			_stateMachine = new StateMachine();

			var navMeshAgent = GetComponent<NavMeshAgent>();
			var animator = GetComponent<Animator>();

			var FindNodeState = new FindNodeState(this);
			var WalkToNodeState = new WalkToNodeState(this, navMeshAgent, animator);
			var InteractWithGroupNode = new InteractWithGroupNodeState(this);

			AddTransition(FindNodeState, WalkToNodeState, HasTargetNode());
			AddTransition(WalkToNodeState, InteractWithGroupNode, TargetNodeIsGroupNode());
			AddTransition(InteractWithGroupNode, FindNodeState, NotInteracting());

			AddTransition(WalkToNodeState, FindNodeState, ReachedNode());

			_stateMachine.SetState(FindNodeState);

			void AddTransition(IState from, IState to, Func<bool> predicate) => _stateMachine.AddTransition(from, to, predicate);

			Func<bool> HasTargetNode() => () => TargetNode != null;
			Func<bool> TargetNodeIsGroupNode() => () => TargetNode != null && TargetNode is GroupNode && ReachedNode().Invoke();
			Func<bool> ReachedNode() => () =>
												TargetNode != null &&
												Vector3.Distance(transform.position, TargetNode.NodeTransform()) < 1.5f;
			Func<bool> NotInteracting() => () => IsInteracting == false;
		}

		public void Update() => _stateMachine.Tick();
	}

}
