﻿using Assets.AI.Entity;
using System.Linq;
using UnityEngine;

namespace Assets.AI.States
{
	public class FindNodeState : IState
	{
		private readonly Civilian _civilian;

		public FindNodeState(Civilian civilian)
		{
			_civilian = civilian;
		}

		public void OnEnter()
		{
		}

		private INode FindNode()
		{
			return Object.FindObjectsOfType<MonoBehaviour>().OfType<INode>()
			.Where(t => t.Condition() == true)
			.OrderBy(t => Random.Range(0, int.MaxValue))
			.FirstOrDefault();
		}

		public void OnExit()
		{
		}

		public void Tick()
		{
			_civilian.TargetNode = FindNode();
		}
	}
}
