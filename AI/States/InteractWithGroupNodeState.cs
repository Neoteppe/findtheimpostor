﻿using Assets.AI.Entity;
using UnityEngine;

namespace Assets.AI.States
{
	public class InteractWithGroupNodeState : IState
	{
		private float _duration;
		private GroupNode _node;
		private readonly Civilian _civilian;

		public InteractWithGroupNodeState(Civilian civilian)
		{
			_civilian = civilian;
		}

		public void OnEnter()
		{
			if (!ShouldEnterGroup()) return;
				
			_node = (GroupNode)_civilian.TargetNode;
			_node.CurrentGroupCount++;
			_duration = Random.Range(0, 20);
			_civilian.IsInteracting = true;
		}
		private bool ShouldEnterGroup()
		{
			int roll = Random.Range(0, 100);
			return roll >= 70;
		}

		public void OnExit()
		{ 
			if(_node != null)
				_node.CurrentGroupCount--;
		}

		public void Tick()
		{
			_duration -= Time.deltaTime;

			if (_duration <= 0)
			{
				_civilian.IsInteracting = false;
			}
		}
	}
}
