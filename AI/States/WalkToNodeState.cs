﻿using Assets.AI.Entity;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.AI.States
{
	public class WalkToNodeState : IState
	{
		private readonly Civilian _civilian;
		private readonly NavMeshAgent _agent;
		private readonly Animator _animator;

		public WalkToNodeState(Civilian civilian ,NavMeshAgent agent, Animator animator)
		{
			_civilian = civilian;
			_agent = agent;
			_animator = animator;
		}

		public void OnEnter()
		{
			_agent.enabled = true;
			_animator.SetBool("Walk", true);
			_animator.SetFloat("Speed", 1);
			_agent.SetDestination(_civilian.TargetNode.NodeTransform());
			_civilian.WaitTimer = 0;
		}

		public void OnExit()
		{
			_agent.enabled = false;
			_animator.SetBool("Walk", false);
			_animator.SetFloat("Speed", 0);
		}

		public void Tick()
		{
			
         if (!_agent.hasPath)
			{
				return;
			}
		}
	}
}